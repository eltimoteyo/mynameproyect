//import Vue from 'vue'
import * as MutationTypes from './mutation-types'

const state = {
  menu: []
  //user: User.from(session.getCookie('tokenSessionServer'))
}

const mutations = {
  [MutationTypes.OBTENER_MENU] (state, menu) {
    state.menu = menus;
  },
  [MutationTypes.ELIMINAR_MENU] (state) {
    state.user = []
  }
}

const getters = {
  currentMenu (state) {
          return state.menu;
  }
};

const actions = {

  obtenerMenu ({ commit, state }, menu) {
    if (menu.length === 0) {
        commit(MutationTypes.OBTENER_MENU, menu);
     }
  },
  EliminarMenu ({ commit }) {
    commit(MutationTypes.ELIMINAR_MENU)
  }
};
export default  {
  state,
  mutations,
  getters,
  actions
}