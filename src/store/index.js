import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
//import getMenuPermisos from './getMenuPermisos'
import storecotizar from './storecotizar'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    //getMenuPermisos,
    storecotizar
  }
})
