/* global localStorage */
//import router from '../router'
import User from '@/models/User'
import * as MutationTypes from './mutation-types'
var session = require('./getSession.js');
//console.log(router.app.$session.exists())
const state = {
  //user: User.from(localStorage.token)
  user: User.from(session.getCookie('tokenSessionServer'))
}

const mutations = {
  [MutationTypes.LOGIN] (state) {
    state.user = User.from(session.getCookie('tokenSessionServer'))
  },
  [MutationTypes.LOGOUT] (state) {
    state.user = null
  }
}

const getters = {
  currentUser (state) {
        var data = new Object();
        const user=state.user;
        if (user) {
          data.IdUsuario=user.nameid;
          data.DescUsuario=user.unique_name;
          data.IdAgencia=user.actort;
          data.DescAgencia=user.family_name;
          data.UrlLogoAgencia=user.role;
        }else {
          data=null;
        }
    return data
  }
}

const actions = {
  login ({ commit }) {
    commit(MutationTypes.LOGIN)
  },

  logout ({ commit }) {
    commit(MutationTypes.LOGOUT)
  }
}

export default  {
  state,
  mutations,
  getters,
  actions
}
