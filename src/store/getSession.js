

module.exports.getCookie = function (cname) { 
      var name = utf8_to_b64( cname ) + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};
module.exports.deleteCookie = function (cname) { 
      var name = utf8_to_b64( cname );
      document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      //return "";
};
// var delete_cookie = function(name) {
//     document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
// };
function utf8_to_b64( str ) {
  return window.btoa(unescape(encodeURIComponent( str )));
}