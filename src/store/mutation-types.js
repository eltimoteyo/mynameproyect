export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
/*************MENU*******/
export const OBTENER_MENU = 'OBTENER_MENU'
export const ELIMINAR_MENU = 'ELIMINAR_MENU'
/*************COTIZACIONES*******/
export const ADDCOTI = 'ADDCOTI'
export const DELETECOTI = 'DELETECOTI'
export const STATUSUPDATE = 'STATUSUPDATE'
