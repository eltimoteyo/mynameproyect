import * as MutationTypes from './mutation-types'

const state = {
  coti: {
            cotititle:'no ola mundo',
            cotiitems:[],
            cotisatus:false
      }
  //user: User.from(session.getCookie('tokenSessionServer'))
}

const mutations = {
  [MutationTypes.OBTENER_MENU] (state, coti) {
    state.coti = coti;
  },
  [MutationTypes.ELIMINAR_MENU] (state) {
    state.coti = []
  }
}

const getters = {
  currentCoti (state) {
          return state.coti;
  }
};

const actions = {

  addCoti ({ commit, state }, coti) {
    //console.log(coti);
    if (coti.length === 0) {
        commit(MutationTypes.OBTENER_MENU, coti);
     }
  },
  EliminaCoti ({ commit }) {
    commit(MutationTypes.ELIMINAR_MENU)
  }
};
export default  {
  state,
  mutations,
  getters,
  actions
}
