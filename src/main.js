import '@babel/polyfill'
import Vue from 'vue'
//import VueRouter from 'vue-router'
import store from './store'
import router from './router'
import './plugins/vuetify'
import App from './App.vue'
//import axios from '@/backend/vue-axios'
import Popover from 'vue-js-popover'
import iziToast from '@/plugins/iziToastPlugin'

import VueAxios from 'vue-axios'

import axios from '@/backend/vue-axios/axios'

Vue.use(VueAxios, axios)
Vue.use(Popover)
// var options = {
//     persist: true
//}
// axios.interceptors.response.use((response) => {
//     return response;
// }, function (error,status) {
//     // Do something with response error
//     const resp= error.headers;
//     console.log(JSON.stringify(resp));
//     if (error.response.status === 401) {
//         console.log('unauthorized, logging out ...');
//         auth.logout();
//         router.replace('/login');
//     }
//     return Promise.reject(error.response);
// });

// 
axios.interceptors.response.use(
    (response) => {
      console.log(response);
      return response
  	},
     (error) => {
       const originalRequest = error.response
       console.log(error);
       if (error.response.status === 401 && error.response.data.error == "token_expired") {
         originalRequest._retry = true
         //store.dispatch('refreshToken').then((response) => {
         console.log(error);
      //   let token = response.data.token
      //   let headerAuth = 'Bearer ' + response.data.token
      //   store.dispatch('saveToken', token)
      //   axios.defaults.headers['Authorization'] = headerAuth
      //   originalRequest.headers['Authorization'] = headerAuth
      //   return axios(originalRequest)
      // }).catch((error) => {
      //   store.dispatch('logout').then(() => {
      //     router.push({ name: 'login' })
      //   }).catch(() => {
      //     router.push({ name: 'login' })
      //   })
      // })
    }
    return Promise.reject(error)
  }
);



Vue.config.productionTip = true
Vue.use(iziToast)

// router.beforeEach((to, from, next) => {
//   const reqSession = to.matched.some(route => route.meta.requiresSession)
//   if (!reqSession){
//   	next()
//   } 

//   // if(localStorage.token){
//   //   next()
//   // } else {
//   //   //next({ name: 'Login' })
//   //   //router.app.$session.exists()
//   //   router.push('/?redirect=' + router.app.$router.path)
//   // }
// });
new Vue({
  router,
  //axios,
  store,
  render: h => h(App)
}).$mount('#app')
