/* global localStorage */

import axios from 'axios'
var session = require('../../store/getSession.js');

const API_URL = process.env.API_URL || 'https://samiapi.azurewebsites.net/api'

const header = {
    'accept': 'application/json',
    'Content-Type':'application/json'
    //'content-type': 'application/x-www-form-urlencoded',
    //'X-Requested-With': 'XMLHttpRequest'
};
if (session.getCookie('tokenSessionServer') !='') {
	header.Authorization = 'Bearer ' + session.getCookie('tokenSessionServer');
}
export default axios.create({
  baseURL: API_URL,
  headers: header
})
