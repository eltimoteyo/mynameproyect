import JwtDecode from 'jwt-decode'

export default class User {
  static from (token) {
    try {
      let obj = JwtDecode(token)
      return new User(obj)
    } catch (_) {
      delete localStorage.token
      return null
    }
  }

  constructor ({ nameid, unique_name,actort,family_name,role}) {
    this.nameid = nameid // eslint-disable-line camelcase
    this.unique_name = unique_name
    this.actort = actort
    this.family_name = family_name
    this.role = role
  }

  get isAdmin () {
    return this.admin
  }
}
