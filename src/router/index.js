import Vue from 'vue'
//import axios from '../backend/vue-axios/index'
import Router from 'vue-router'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
import Master from '@/components/Master'
import Home from '@/components/Master/Home'
import Usuario from '@/components/Master/Usuario'
import Trabajador from '@/components/Master/Trabajador'
import Perfil from '@/components/Master/Perfil'
import Cliente from '@/components/Master/Cliente'
import Cotizaciones from '@/components/Master/Cotizaciones'
import Cotizador from '@/components/Master/Cotizador'
import Settings from '@/components/Master/Settings'
//import ListaAgencias from '@/components/Master/ListaAgencias'
        // axios.get('https://jsonplaceholder.typicode.com/posts/2')///http://localhost:1067/api/Products
        // //axios.get('http://localhost:1067/api/Products')
        // // axios.get('http://engine.ctmtoursperu.com:80/cars/categories')
        // .then((resouesta)=>{
        //   //this.restaurantes=resouesta.data;
        //  console.log('estoy en :');
        //  });

Vue.use(Router)
export default new Router({
  base: '/mynameproyect',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Master',
      component: Master,
      children: [
        {
          path: '/home',
          component: Home,
          name: 'Home',
          meta: {title: 'Administracion home'}
        },
  	    {
  	      path: '/usuario',
  	      name: 'Usuario',
  	      component: Usuario,
          meta: {
            requiresSession: false
              }
  	    },
        {
          path: '/trabajador',
          name: 'Trabajador',
          component: Trabajador,
          meta: {
            requiresSession: false
              }
        },
        {
          path: '/perfil',
          name: 'Perfil',
          component: Perfil,
          meta: {
            requiresSession: false
              }
        },
         {
          path: '/cliente',
          name: 'Cliente',
          component: Cliente,
          meta: {
            requiresSession: false
              }
        },
        {
          path: '/settings',
          name: 'Settings',
          component: Settings,
          meta: {
            requiresSession: false
              }
        },
        {
          path: '/cotizaciones',
          name: 'Cotizaciones',
          component: Cotizaciones,
          meta: {
            requiresSession: false
              }
        }
	    ]
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    }
  ]
})